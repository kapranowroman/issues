import { configureStore } from '@reduxjs/toolkit';
import issueListReducer from '../redux/issueListSlice';
import filterReducer from '../redux/filterSlice';

export default configureStore({
  reducer: {
    issues: issueListReducer,
    filter: filterReducer,
  },
});

import React from 'react';
import {createUseStyles} from "react-jss";

const useStyles = createUseStyles({
    filterWrapper: {
        border: '1px solid #e4e4e4',
        borderRadius: '15px',
        backgroundColor: '#fff',
        height: '30px',
        margin: '10px 0 5px 0',
        '& input': {
            border: 'none',
            background: 'none',
            margin: '0 15px'
        },
        '& input:focus':{
            outline: 'none'
        }
    }
})

export function Filter({value = '', onChange, placeholder = ''}) {
    const classes = useStyles();
    return (
        <div className={classes.filterWrapper}>
            <input type={'text'} value={value} placeholder={placeholder} onChange={(e) => onChange(e.target.value)}/>
        </div>
    )
}
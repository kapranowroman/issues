import React from "react";
import {createUseStyles} from 'react-jss'

const useStyles = createUseStyles({
    tagWrapper: {
        borderRadius: '14px',
        background: '#9e9e9e',
        color: 'white',
        display: 'flex',
        justifyContent: 'center',
        padding: '0 10px',
        margin: '2px 2px',
    },
    text: {
        margin: '3px',
        lineHeight: '18px',
        fontSize: '14px'
    }
})


export function Tag({text = ''}) {
    const classes = useStyles();

    return (
        <div className={classes.tagWrapper}><span className={classes.text}>{text.toUpperCase()}</span></div>
    )
};
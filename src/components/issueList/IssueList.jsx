import React from "react";
import {IssueItem} from "../issueItem/IssueItem";
import {createUseStyles} from "react-jss";
const useStyles = createUseStyles({
    issuesWrapper: {
        display: 'flex',
        width: '100%',
        justifyContent: 'center',
        flexDirection: 'row',
        flexWrap: 'wrap',
    }
})

export function IssueList({issues=[], onSelect}) {
    const classes = useStyles();
return (
    <div className={classes.issuesWrapper}>
        {issues.map(i=><IssueItem issue={i} onSelect={onSelect} key={i.id}/>)}
    </div>
)
}
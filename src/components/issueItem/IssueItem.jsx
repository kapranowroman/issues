import React, {useState} from "react";
import {Tag} from "../tag/Tag";
import {createUseStyles} from 'react-jss'

const useStyles = createUseStyles({
    issueWrapper: {
        position: 'relative',
        width: '37%',
        display: 'flex',
        flexDirection: 'column',
        borderRadius: '10px',
        alignItems: 'flex-start',
        backgroundColor: '#fff',
        margin: '7px',
        padding: '18px',
        '&:hover': {
            cursor: 'pointer'
        }
    },
    hovered: {
        zIndex: '2',
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
        boxShadow: '0 0 10px rgba(0,0,0,0.5)',
        display: 'flex',
        flexDirection: 'column',
        borderRadius: '10px',
        alignItems: 'flex-start',
        backgroundColor: '#fff',
        padding: '18px 0',
    },
    issueContentWrapper: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-start',
    },
    tagListWrapper: {
        display: 'flex',
        flexWrap: 'wrap',
        padding: '15px 0'
    },
    withMargin: {
        margin: '0 18px',
        textAlign: 'left',
        width: 'calc(100% - 36px)',
    },
    clientName: {
        height: '45px',
        width: '100%',
        display: 'flex',
        alignItems: 'center',
        backgroundColor: '#e4e4e4',
    },
    amount: {
        marginBottom: '10px',
    },
    companyInn: {
        fontSize: '14px',
        fontWeight: 500,
        color: '#cccccc'
    },
    header: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'baseline',
        marginBottom: '10px',
        '& span': {
            fontWeight: 600,
            textAlign: 'left',
        }
    },
    footer: {
        display: 'flex',
        '& span': {
            fontSize: '14px',
            fontWeight: 500,
            color: '#cccccc'
        },
    },
    openToUp:{
        top:'unset',
        bottom:0,
    },
    '@media screen and (min-width: 1024px)': {
        issueWrapper: {
            width: 'unset'
        }
    },
    '@media screen and (max-width: 414px)': {
        issueWrapper: {
            width: '100%'
        }
    }
})
const toUp = (e) => {
    return document.body.getBoundingClientRect().top + document.body.scrollHeight - (e.target.getBoundingClientRect().top + e.target.scrollHeight) < e.target.scrollHeight * 2.5;
}

export function IssueItem({issue = {}, onSelect}) {
    const [isHovered, setIsHovered] = useState(false);
    const [openToUp, setOpenToUp] = useState(false);
    const classes = useStyles();
    const {
        id,
        header,
        amount,
        currency,
        companyName,
        companyInn,
        clientName,
        date,
        tags,
        statuses,
        selected
    } = issue;

    const handleOnMouseEnter = (e) => {
        if (toUp(e)) {
            setOpenToUp(true);
        }
        setIsHovered(true)
    }

    return (
        <div className={classes.issueWrapper} onMouseEnter={handleOnMouseEnter}
             onMouseLeave={() => setIsHovered(false)}>
            <div className={`${classes.issueContentWrapper}`}>
                <div className={classes.header}>
                    <span>{header}</span>
                </div>
                <div className={classes.amount}>
                    <span>{`${new Intl.NumberFormat('ru-RU').format(amount)} ${currency}`}</span>
                </div>
                <div>
                    <span>{companyName}</span>
                </div>
                <div className={classes.companyInn}>
                    <span>{`ИНН ${companyInn}`}</span>
                </div>
                <div className={classes.footer}>
                    <span> {`${id} от ${date}`}</span>
                </div>
            </div>
            {isHovered ? <div className={`${classes.hovered} ${openToUp? classes.openToUp:''}`}>
                <div className={`${classes.header} ${classes.withMargin}`}>
                    <span>{header}</span>
                    <input type={'checkbox'} checked={selected} onChange={onSelect}/>
                </div>
                <div className={`${classes.withMargin} ${classes.amount}`}>
                    <span>{`${new Intl.NumberFormat('ru-RU').format(amount)} ${currency}`}</span>
                </div>
                <div className={`${classes.withMargin}`}>
                    <span>{companyName}</span>
                </div>
                <div className={`${classes.withMargin} ${classes.companyInn}`}>
                    <span>{`ИНН ${companyInn}`}</span>
                </div>
                <div className={`${classes.tagListWrapper} ${classes.withMargin}`}>
                    {statuses.map((i, ind) => <Tag text={i} key={ind}/>)}
                </div>
                <div className={classes.clientName}>
                    <span className={`${classes.withMargin}`}>{clientName}</span>
                </div>
                <div className={`${classes.tagListWrapper} ${classes.withMargin}`}>
                    {tags.map((i, ind) => <Tag text={i} key={ind}/>)}
                </div>
                <div className={`${classes.withMargin} ${classes.footer}`}>
                    <span>{`${id} от ${date}`}</span>
                </div>
            </div> : ''}
        </div>
    )
}
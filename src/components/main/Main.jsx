import React, {useState, useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {Filter} from "../filter/Filter";
import {createUseStyles} from 'react-jss';
import {IssueList} from "../issueList/IssueList";
import {selectFilteredIssues, updateIssues} from "../../redux/issueListSlice";
import {
    selectFilterByName,
    selectFilterByNumber,
    updateFilterByName,
    updateFilterByNumber
} from "../../redux/filterSlice";

const useStyles = createUseStyles({
    mainWrapper: {
        width: '100%',
        height: '100%',
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column'
    },
    filterWrapper: {
        width: '85%',
        flexWrap: 'nowrap',
        display: 'flex',
        justifyContent: 'flex-start',
        '& div': {
            marginRight: '5px',
            maxWidth: '180px'
        }
    },
    '@media screen and (max-width: 414px)': {
        filterWrapper: {
            width: '95%',
            flexWrap: 'wrap'
        }
    }
})

export function Main() {
    const issuesList = useSelector(selectFilteredIssues);
    const numberForFilter = useSelector(selectFilterByNumber);
    const nameForFilter = useSelector(selectFilterByName);
    const dispatch = useDispatch();
    useEffect(() => {
        const fetchIssues = async () => {
            try {
                const response = await fetch('/api/getIssues');
                const json = await response.json();
                dispatch(updateIssues(json));
            } catch (error) {
                console.error('Ошибка:', error);
            }
        };
        fetchIssues();
    }, []);

    const classes = useStyles();
    return (
        <div className={classes.mainWrapper}>
            <div className={classes.filterWrapper}>
                <Filter value={numberForFilter} placeholder={'Номер заявки'} onChange={(val) => {
                    dispatch(updateFilterByNumber(val))
                }}/>
                <Filter value={nameForFilter} placeholder={'Наименование клиента'} onChange={(val) => {
                    dispatch(updateFilterByName(val))
                }}/>
            </div>
            <IssueList issues={issuesList}/>
        </div>
    )
}
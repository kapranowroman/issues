import { createSlice } from '@reduxjs/toolkit';

export const filterSlice = createSlice({
    name: 'filter',
    initialState: {
        byName:'',
        byNumber:'',
    },
    reducers: {
        updateFilterByName: (state, action) => {
            state.byName = action.payload;
        },
        updateFilterByNumber: (state, action) => {
            state.byNumber = action.payload;
        },
    },
});

export const { updateFilterByName, updateFilterByNumber } = filterSlice.actions;

export const selectFilterByName = state => state.filter.byName;
export const selectFilterByNumber = state => state.filter.byNumber;

export default filterSlice.reducer;
import {createSlice} from '@reduxjs/toolkit';

export const issueListSlice = createSlice({
    name: 'issues',
    initialState: {
        issuesList: []
    },
    reducers: {
        updateIssues: (state, action) => {
            state.issuesList = action.payload;
        },
    },
});

export const {updateIssues} = issueListSlice.actions;

export const selectFilteredIssues = state => {
    const {byName, byNumber} = state.filter;
    const res = state.issues.issuesList.filter((i) =>{
            if (!!byNumber&&i.id.includes(byNumber)){
                return true;
            }
            return !byNumber;
        }).filter(i=>{
        if (!!byName&&i.companyName.toLowerCase().includes(byName.toLowerCase())){
            return true;
        }
        return !byName;
    });
    return res;
}

export default issueListSlice.reducer;
